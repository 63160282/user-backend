export class User {
  id: number;
  login: string;
  name: string;
  password: string;
}

export class Product {
  id: number;
  name: string;
  price: number;
}
